#!/bin/sh
function not_empty() {
	[ "$(ls -A ""$1"")" ]
}
set -e
TARGET_JAR_FOLDER='deb_package/usr/bin/mjk-sync-notifier'
mkdir -p "$TARGET_JAR_FOLDER"
mvn clean package
# Clearing the target folder
find "$TARGET_JAR_FOLDER" -mindepth 1 -delete

cp target/backup-notifier*.jar "$TARGET_JAR_FOLDER/mjk-sync-notifier.jar"
cp target/lib -r "$TARGET_JAR_FOLDER/lib"
dpkg -b deb_package/ mjk-sync-notifier.deb
