module com.gitlab.majok.syncnotifier {
	opens com.gitlab.majok.syncnotifier to javafx.graphics;
	requires java.desktop;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires SystemTray;
	requires javafx.controls;

}
