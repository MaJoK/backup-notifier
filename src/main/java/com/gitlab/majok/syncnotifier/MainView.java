package com.gitlab.majok.syncnotifier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainView extends VBox {
	@SuppressWarnings("unused")
	private static final Logger logger = LogManager.getLogger();
	private final Label statusLabel, lastSync, nextSync;
	private final Button syncStart;
	private final TextArea syncOutput;
	private final ImageView loadingGif;
	private Button openOutput;
	private final Stage outputStage;

	public MainView(Stage owner) {
		// Adding the title

		{
			Label title = new Label("Sync Notifier");
			title.setFont(new Font(30));
			getChildren().add(title);
		}
		{
			statusLabel = new Label();
			statusLabel.setPrefWidth(340);
			statusLabel.setTextAlignment(TextAlignment.CENTER);
			statusLabel.setAlignment(Pos.BASELINE_CENTER);
			lastSync = new Label();
			nextSync = new Label();
		}
		// Output textarea
		{
			openOutput = new Button("Show Output");
			openOutput.setVisible(false);
			syncOutput = new TextArea();
			syncOutput.setEditable(false);
			syncOutput.setPrefSize(400, 600);
			syncOutput.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

			outputStage = new Stage();
			outputStage.initOwner(owner);
			outputStage.initModality(Modality.NONE);
			StackPane pane = new StackPane(syncOutput);
			pane.setPadding(new Insets(10));
			outputStage.setScene(new Scene(pane));
			outputStage.setOnCloseRequest(windowEvent -> outputStage.hide());
			openOutput.setOnAction(actionEvent -> outputStage.show());
		}

		StackPane bottom = new StackPane();

		{
			loadingGif = new ImageView(getClass().getResource("/loading-ring.gif").toString());
			syncStart = new Button("Start Sync now");
			bottom.getChildren().addAll(loadingGif, syncStart);
			loadingGif.setVisible(false);

			loadingGif.setFitHeight(125);
			loadingGif.setFitWidth(125);
		}

		getChildren().addAll(statusLabel, lastSync, nextSync, openOutput, bottom);
		setSpacing(10);
		setPadding(new Insets(10));

		setAlignment(Pos.CENTER);
		setMinWidth(USE_PREF_SIZE);
	}

	public void showOpenOutput(boolean show) {
		openOutput.setVisible(show);
	}

	public void showButtons(boolean show) {
		syncStart.setVisible(show);
		openOutput.setVisible(show);
	}

	public void setStatus(String status, boolean error) {
		statusLabel.setText(status);
		if (error)
			statusLabel.setTextFill(Color.RED);
		else
			statusLabel.setTextFill(Color.BLACK);
	}

	public void setNextSync(String status) {
		nextSync.setText(status);
	}

	public void setLastSync(String status) {
		lastSync.setText(status);
	}

	public void setSyncRunning(boolean syncRunning) {
		syncStart.setVisible(!syncRunning);
		loadingGif.setVisible(syncRunning);
	}

	public Button getSyncStartButton() {
		return syncStart;
	}

	public TextArea getSyncOutputTextArea() {
		return syncOutput;
	}
}