package com.gitlab.majok.syncnotifier;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.EntryMessage;

import dorkbox.systemTray.Menu;
import dorkbox.systemTray.MenuItem;
import dorkbox.systemTray.SystemTray;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	private static final Path dataFolder = Paths.get(System.getenv("HOME"), ".local/share/mjk-sync-notifier");
	private static final Logger logger = LogManager.getLogger();
	private static final String rsyncSourceKey = "rsync-source", rsyncDestKey = "rsync-dest",
			rsyncOptionsKey = "rsync-options", scheduleMinutesKey = "schedule-minutes",
			configPropertiesFileName = "config.properties", lastRunFile = "lastrun";
	private boolean configLoaded;
	private String errorMessage;
	private Properties config;
	private MainView mview;
	private Instant lastRun;
	private boolean isSyncing;
	private ScheduledFuture<?> scheduleTask;
	private int scheduleMinutes;
	private Stage stage;
	private SystemTray sysTray;
	private BufferedImage cloudImage;
	private BufferedImage uploadCloudImage;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		mview = new MainView(primaryStage);
		primaryStage.setScene(new Scene(mview));
		primaryStage.setTitle("mjk Sync Notifier");
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/cloud.png")));
		configLoaded = tryLoadConfig();
		if (!configLoaded) {
			mview.setStatus(errorMessage, true);
			mview.showButtons(false);
		} else {
			initSyncing();
			if (SystemTray.get() != null) {
				logger.debug("SystemTray support is given");
				initSysTrayIcon();
				primaryStage.setOnCloseRequest(windowEvent -> primaryStage.hide());
				Platform.setImplicitExit(false);

			} else {
				logger.debug("No SystemTray support is given");
			}
		}
		if (!getParameters().getRaw().contains("--start-hidden"))
			primaryStage.show();
	}

	private void initSysTrayIcon() throws IOException {
		sysTray = SystemTray.get();
		cloudImage = ImageIO.read(getClass().getResource("/cloud.png"));
		uploadCloudImage = ImageIO.read(getClass().getResource("/cloud_upload.png"));
		// Icon icon = new ImageIcon(image);
		sysTray.setImage(cloudImage);
		sysTray.setTooltip("Sync-Notifier");
		Menu menu = sysTray.getMenu();

		menu.setImage(cloudImage);
		MenuItem open = new MenuItem("Open");

		open.setCallback(actionEvent -> {
			logger.debug("Open callback called.");
			Platform.runLater(() -> {
				stage.show();
			});
		});

		MenuItem quit = new MenuItem("Quit");
		quit.setCallback(actionEvent -> Platform.exit());
		menu.add(open);
		menu.add(quit);
		sysTray.setEnabled(true);
	}

	private void initSyncing() throws IOException {
		EntryMessage entryM = logger.traceEntry("initSyncing");
		readLastRun();
		scheduleMinutes = Math.max(0, Integer.parseInt(config.getProperty(scheduleMinutesKey)));

		long delay;
		if (lastRun.isBefore(Instant.now().minusSeconds(scheduleMinutes * 60))) {
			logger.debug("Last run is longer than schedule minutes ago, so not triggering sync immediately");
			delay = 0;
		} else {
			logger.debug("Last run is shorter than schedule minutes ago");
			delay = lastRun.plus(scheduleMinutes, ChronoUnit.MINUTES).getEpochSecond() - lastRun.getEpochSecond();
			delay /= 60;// Minutes
		}

		ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor(run -> {
			Thread t = new Thread(run);
			t.setDaemon(true);
			t.setName("sync-schedule-and-run");
			return t;
		});

		scheduleTask = execService.scheduleAtFixedRate(() -> {
			syncIfNotAlreadySyncing();
		}, delay, scheduleMinutes, TimeUnit.MINUTES);

		mview.getSyncStartButton().setOnAction(actionEvent -> {
			scheduleTask.cancel(false);
			scheduleTask = execService.scheduleAtFixedRate(() -> {
				try {
					syncIfNotAlreadySyncing();
				} catch (Exception e) {
					logger.error("Unexpeced Exception occured while syncing", e);
				}
			}, 0, scheduleMinutes, TimeUnit.MINUTES);
		});
		if (delay > 0) {
			mview.setStatus("", false);
			mview.setLastSync("Previous Sync was at " + getTimeString(lastRun));
		}
		logger.traceExit(entryM);
	}

	private void readLastRun() throws IOException {
		EntryMessage entryM = logger.traceEntry("readLastRun");
		if (Files.exists(dataFolder.resolve(lastRunFile))) {
			try (BufferedReader lastRunReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(dataFolder.resolve(lastRunFile).toFile())))) {
				String lastRunString = lastRunReader.readLine();
				lastRun = Instant.parse(lastRunString);
			}
		} else {
			logger.warn("Last run file doesn't exist, using 1970 as last run");
			lastRun = Instant.ofEpochMilli(0);
		}
		logger.traceExit(entryM);
	}

	private void saveLastRun() throws IOException {
		EntryMessage entryM = logger.traceEntry("saveLastRun");
		String lr = lastRun.toString();
		FileOutputStream out = new FileOutputStream(dataFolder.resolve(lastRunFile).toFile());
		out.write(lr.getBytes());
		out.flush();
		out.close();
		logger.traceExit(entryM);
	}

	private boolean tryLoadConfig() {
		EntryMessage entryM = logger.traceEntry("tryLoadConfig");
		logger.info("Data folder is {}", dataFolder.toAbsolutePath().toString());

		dataFolder.toFile().mkdirs();
		Path configFile = dataFolder.resolve(configPropertiesFileName);

		if (!Files.exists(configFile)) {
			errorMessage = "Config Properties File doesn't exist!";
			logger.error("Config file doesn't exist");
			return logger.traceExit(entryM, false);
		}

		// trying to read the content of the file
		Properties config = new Properties();
		try {
			config.load(new FileInputStream(configFile.toFile()));
		} catch (IOException e) {
			errorMessage = "Error while trying open the config file: " + e.getMessage();
			logger.error("Error while opening the config file", e);
			return logger.traceExit(entryM, false);
		}

		// Properties are loaded, checking whether everything is present:
		if (!(config.containsKey(rsyncDestKey) && config.containsKey(rsyncOptionsKey)
				&& config.containsKey(rsyncSourceKey) && config.containsKey(scheduleMinutesKey))) {
			errorMessage = "At least one property is missing in the properties file!";
			logger.error("Error while reading the config file");
			return logger.traceExit(entryM, false);
		}

		if (!config.getProperty(scheduleMinutesKey).matches("\\d+")) {
			errorMessage = "Property " + scheduleMinutesKey + " is no valid positive number";
			logger.error(errorMessage);
			return logger.traceExit(entryM, false);
		}
		this.config = config;
		logger.debug("Assigned the sucessful config");
		return logger.traceExit(entryM, true);
	}

	private void syncIfNotAlreadySyncing() {
		EntryMessage entryM;
		synchronized (this) {
			entryM = logger.traceEntry("syncIfNotAlreadySyncing");
			if (isSyncing) {
				logger.debug("Another thread is already syncing");
				logger.traceExit(entryM);
				return;
			}
			isSyncing = true;
		}
		{
			final Instant prevRun = lastRun;
			Platform.runLater(() -> {
				mview.showOpenOutput(true);
				mview.setLastSync("Previous Sync was at " + getTimeString(prevRun));
				mview.setNextSync("");
			});
		}
		lastRun = Instant.now();
		try {
			saveLastRun();
		} catch (IOException e) {
			logger.error("Error while saving last run!", e);
		}

		doSync(() -> {
			mview.setLastSync("Last Sync was at " + getTimeString(lastRun));
			mview.setNextSync(
					"Next Sync will be at " + getTimeString(lastRun.plus(scheduleMinutes, ChronoUnit.MINUTES)));
			synchronized (this) {
				isSyncing = false;
			}
		});
		logger.traceExit(entryM);
	}

	private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	private static final String getTimeString(final Instant i) {
		final LocalDateTime dt = LocalDateTime.ofInstant(i, ZoneId.systemDefault());
		return dt.format(timeFormatter);
	}

	private void doSync(Runnable onFinished) {
		EntryMessage entryMessage = logger.traceEntry("doSync Method");
		final TextArea outputTA = mview.getSyncOutputTextArea();
		logger.info("Starting sync");

		Platform.runLater(() -> {
			mview.setSyncRunning(true);
			mview.setStatus("Syncing since " + getTimeString(lastRun), false);
		});

		if (sysTray != null) {
			sysTray.setImage(uploadCloudImage);
		}

		var rsyncRunner = new RsyncRunner(output -> {
			Platform.runLater(() -> outputTA.appendText(output));
		}, getCommandParameters(), exitCode -> {
			String termData = "rsync terminated with " + exitCode;
			logger.info(termData);
			Platform.runLater(() -> {
				outputTA.appendText("\n--------------------------------------------------------------\n" + termData);
				mview.setSyncRunning(false);

				if (exitCode == 0) {
					if (sysTray != null)
						sysTray.setStatus("Last Sync ran successful");
					mview.setStatus("Sync finished.", false);
				} else {
					mview.setStatus("Error while trying to sync :(, please see the output.", true);
					stage.show();
				}
				onFinished.run();
			});
		});
		String command = rsyncRunner.getInvokedCommand();
		logger.info("Calling: \"{}\"", command);

		Platform.runLater(() -> outputTA
				.setText("Command: " + command + "\n--------------------------------------------------------------\n"));
		try {
			rsyncRunner.start();
		} catch (IOException e) {
			logger.error("Error while invoking rsync process", e);
			Platform.runLater(() -> outputTA.setText("Error while invoking rsync process:\n" + e));
			if (sysTray != null)
				sysTray.setStatus("Error while invoking rsync process");
		} finally {
			logger.traceExit(entryMessage);
			if (sysTray != null)
				sysTray.setImage(cloudImage);
		}
	}

	private ArrayList<String> getCommandParameters() {
		String options = config.getProperty(rsyncOptionsKey);
		String source = config.getProperty(rsyncSourceKey);
		String dest = config.getProperty(rsyncDestKey);
		ArrayList<String> callParam = new ArrayList<>();
		callParam.add("rsync");
		for (String option : options.split(" "))
			callParam.add(option);
		callParam.add(source);
		callParam.add(dest);
		return callParam;
	}
}