package com.gitlab.majok.syncnotifier;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RsyncRunner {
	private static final Logger logger = LogManager.getLogger();
	private final OutputSink outputSink;
	private final ProcessFinishedListener finishedListener;
	private ProcessBuilder preparedProcessBuilder;
	String invokedCommand;

	@FunctionalInterface
	static interface ProcessFinishedListener {
		void processFinished(int exitCode);
	}

	@FunctionalInterface
	static interface OutputSink {
		void newOutput(String output);
	}

	public RsyncRunner(OutputSink outputAppender, List<String> args, ProcessFinishedListener finishedListener) {
		this.outputSink = outputAppender;
		this.finishedListener = finishedListener;
		prepareProcessBuilder(args);
	}

	private void prepareProcessBuilder(List<String> args) {
		int totalLength = 0;
		for (String par : args)
			totalLength += 1 + par.length();
		StringBuilder commandStringBuilder = new StringBuilder(totalLength);
		for (String par : args) {
			commandStringBuilder.append(' ');
			commandStringBuilder.append(par);
		}
		invokedCommand = commandStringBuilder.toString();
		logger.info("Calling: \"{}\"", invokedCommand);

		preparedProcessBuilder = new ProcessBuilder(args);
		preparedProcessBuilder.redirectErrorStream(true);
	}

	public String getInvokedCommand() {
		return invokedCommand;
	}

	public void start() throws IOException {

		Process process = preparedProcessBuilder.start();
		process.getOutputStream().close();
		
		
		var outputReader = new Thread(new OutputReader(process.getInputStream(), outputSink));
		outputReader.setDaemon(true);
		outputReader.start();

		var processExitWaiter = new Thread(() -> {
			try {
				int exitCode = process.waitFor();
				finishedListener.processFinished(exitCode);
			} catch (Exception e) {
				logger.error("Exception while waiting for rsync to terminate: ", e);
			}
		}, "RsyncExitWaiter");
		processExitWaiter.setDaemon(true);
		processExitWaiter.start();
	}

	private static class OutputReader implements Runnable {
		private final InputStream in;
		private final OutputSink sink;

		private OutputReader(InputStream in, OutputSink sink) {
			this.in = in;
			this.sink = sink;
		}

		@Override
		public void run() {
			byte[] buffer = new byte[1024];

			try {
				int read;
				while ((read = in.read(buffer)) != -1) {
					sink.newOutput(new String(buffer, 0, read));
				}
			} catch (IOException e) {
				logger.error("Error while reading from process", e);
			}
		}
	}
}
